Feature: BBC
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario: Check name of headline article
    Given User opens home page
    When User clicks 'News' tab
    Then User checks that name of headline article is 'Reports of survivors emerging from bombed Mariupol theatre'

  Scenario: Check secondary article titles
    Given User opens home page
    When User clicks 'News' tab
    Then User checks that secondary articles titles are correct
      | Russians zombified by propaganda - state TV editor |
      | Russia extends arrest of US basketball star Griner |
      | P&O Ferries sacks 800 but crew refuse to leave ships |
      | 'Is that mummy?' - Nazanin home after Iran ordeal |
      | Iran rescues dozens as cargo ship sinks in Gulf |

  Scenario Outline: Check questions form with empty fields
    Given User opens home page
    And User clicks 'News' tab
    And User clicks 'Coronavirus' tab
    And User clicks 'Your Coronavirus Stories' tab
    And User clicks 'Your questions answered' link
    When User fills form
      | questions     | <question>     |
      | Name          | <name>         |
      | Email address | <emailAddress> |
    And User accepts terms of service
    And User clicks 'Submit' button
    Then User checks that error message is "<errorMessage>"

    Examples:
      | question | name | emailAddress   | errorMessage                 |
      |          | Test | test@gmail.com | can't be blank               |
      | Test     |      | test@gmail.com | Name can't be blank          |
      | Test     | Test |                | Email address can't be blank |

  Scenario: Check questions form with empty accept checkbox
    Given User opens home page
    And User clicks 'News' tab
    And User clicks 'Coronavirus' tab
    And User clicks 'Your Coronavirus Stories' tab
    And User clicks 'Your questions answered' link
    When User fills form
      | questions     | test           |
      | Name          | test           |
      | Email address | test@gmail.com |
    And User clicks 'Submit' button
    Then User checks that checkbox error message is 'must be accepted'

  Scenario Outline: Check scores of football fixtures
    Given User opens home page
    And User clicks 'Sport' tab
    And User clicks 'Football' tab
    And User clicks 'Scores & Fixtures' tab
    When User enters '<championship>' in competition search bar
    And User selects results for '<month>'
    Then User checks that '<firstTeam>' and '<secondTeam>' played with '<fistTeamGoals>' '<secondTeamGoals>' score
    And User clicks '<firstTeam>''<secondTeam>' block
    And User checks that '<firstTeam>' and '<secondTeam>' played with '<fistTeamGoals>' '<secondTeamGoals>' score

    Examples:
      | championship          | month   | firstTeam       | fistTeamGoals | secondTeam        | secondTeamGoals |
      | Scottish Championship | 2021-11 | Dunfermline     | 3             | Ayr United        | 0               |
      | Champions League      | 2021-10 | Atlético Madrid | 2             | Liverpool         | 3               |
      | Championship          | 2021-12 | Blackpool       | 1             | Middlesbrough     | 2               |
      | Scottish League One   | 2021-08 | Dumbarton       | 1             | Cove Rangers      | 3               |
      | Premier League        | 2022-01 | Chelsea         | 2             | Tottenham Hotspur | 0               |


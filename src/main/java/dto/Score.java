package dto;

import lombok.Data;

@Data
public class Score {
    String firstTeamGoals;
    String secondTeamGoals;
}

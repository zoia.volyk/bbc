package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class NewsPage extends BasePage{

    @FindBy(xpath = "//div[@data-entityid='container-top-stories#1']//h3[contains(@class,'title')]")
    private WebElement headlineArticle;

    @FindBy(xpath = "//div[contains(@class,'secondary-item')]//h3[contains(@class,'title')]")
    private List<WebElement> secondaryArticlesList;

    @FindBy(xpath = "//nav[@class='nw-c-nav__wide']//a[contains(@href,'coronavirus')]")
    private WebElement coronavirusTab;

    @FindBy(xpath = "//div[@class='tp-modal']")
    private WebElement registerPopup;

    @FindBy(xpath = "//button[@aria-label='Close']")
    private WebElement closeButton;


    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public String getNameOfHeadlineArticle() {
        return headlineArticle.getText();
    }

    public List<String> getSecondaryArticlesTitlesList() {
        List<String> secondaryArticlesTitles = secondaryArticlesList.stream()
                                                                    .map(WebElement::getText)
                                                                    .collect(Collectors.toList());
        return secondaryArticlesTitles;
    }

    public void clickOnCoronavirusTab() {
        clickOnElement(coronavirusTab);
    }

    public WebElement getRegisterPopup() {
        return registerPopup;
    }

    public void closePopup() {
        clickOnElement(closeButton);
    }
}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronavirusPage extends BasePage{

    @FindBy(xpath = "//nav[@class='nw-c-nav__wide-secondary']//a[contains(@href,'have_your_say')]")
    private WebElement yourCoronavirusStoriesTab;


    public CoronavirusPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnYourCoronavirusStoriesTab() {
        clickOnElement(yourCoronavirusStoriesTab);
    }
}

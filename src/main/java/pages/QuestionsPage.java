package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Map;
import java.util.Optional;

public class QuestionsPage extends BasePage {

    private static final String XPATH_FIELD = "//*[contains(@aria-label,'%s')]";

    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement acceptCheckbox;

    @FindBy(xpath = "//button[contains(text(),'Submit')]")
    private WebElement submitButton;

    @FindBy(xpath = "//div[@class='input-error-message']")
    private WebElement inputErrorMessage;

    @FindBy(xpath = "//div[@class='checkbox']//div[@class='input-error-message']")
    private WebElement checkboxErrorMessage;


    public QuestionsPage(WebDriver driver) {
        super(driver);
    }

    public void clickAcceptCheckbox() {
        clickOnElement(acceptCheckbox);
    }

    public void clickSubmitButton() {
        clickOnElement(submitButton);
    }

    public WebElement getInputErrorMessage() {
        return inputErrorMessage;
    }

    public WebElement getCheckboxErrorMessage() {
        return checkboxErrorMessage;
    }

    public String getTextOfInputErrorMessage() {
        return inputErrorMessage.getText();
    }

    public String getTextOfCheckboxErrorMessage() {
        return checkboxErrorMessage.getText();
    }

    public void fillQuestionForm(Map<String, String> values) {
        values.forEach((k, v) -> {
            if(Optional.ofNullable(v).isEmpty())
                return;
            fillForm(driver.findElement(By.xpath(String.format(XPATH_FIELD, k))), v);
        });
    }
}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SportPage extends BasePage{

    @FindBy(xpath = "//li[contains(@class,'navigation')]/a[@data-stat-title='Football']")
    private WebElement footballTab;


    public SportPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnFootballTab() {
        clickOnElement(footballTab);
    }
}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    private static final String BBC_URL = "https://www.bbc.com";

    @FindBy(xpath = "//nav[@role='navigation']//a[contains(@href,'news')]")
    private WebElement newsTab;

    @FindBy(xpath = "//nav[@role='navigation']//a[contains(@href,'sport')]")
    private WebElement sportTab;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage() {
        driver.get(BBC_URL);
    }

    public void clickOnNewsTab() {
        clickOnElement(newsTab);
    }

    public void clickOnSportTab() {
        clickOnElement(sportTab);
    }
}

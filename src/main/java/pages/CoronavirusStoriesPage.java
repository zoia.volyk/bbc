package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronavirusStoriesPage extends BasePage{

    @FindBy(xpath = "//a[@href='/news/52143212']")
    private WebElement questionsToBbcLink;


    public CoronavirusStoriesPage(WebDriver driver) {
        super(driver);
    }

    public void clickQuestionsToBbcLink() {
        clickOnElement(questionsToBbcLink);
    }
}

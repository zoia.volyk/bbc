package pages;

import dto.Score;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ScoresFixturesPage extends BasePage {

    private static final String CHAMPIONSHIP_XPATH = "//a[contains(@data-reactid,'%s')]";

    private static final String MONTH_XPATH = "//a[contains(@href,'/%s')]";

    private static final String BLOCK_XPATH = "//span[text()='%s']/ancestor::article//span[text()='%s']";

    private static final String GOALS_XPATH = "/ancestor::article//abbr[@title='%s']/../../following-sibling::span/span";

    @FindBy(xpath = "//input[@id='downshift-0-input']")
    private WebElement competitionSearchField;

    @FindBy(xpath = "//span[@class='sp-c-fixture__block']")
    private WebElement yellowBlock;


    public ScoresFixturesPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getCompetitionSearchField() {
        return competitionSearchField;
    }

    public void enterTextToCompetitionSearchField(final String keyword) {
        competitionSearchField.sendKeys(keyword);
        clickOnElement(driver.findElement(By.xpath(String.format(CHAMPIONSHIP_XPATH, keyword))));
    }
    public void clickOnMonthTab(String month) {
        clickOnElement(driver.findElement(By.xpath(String.format(MONTH_XPATH, month))));
    }

    public WebElement getYellowBlock() {
        return yellowBlock;
    }

    public Score getScore(String team1, String team2) {
        Score score = new Score();
        String TEAMS_XPATH = String.format(BLOCK_XPATH, team1, team2);
        if (driver.findElement(By.xpath(TEAMS_XPATH)).isDisplayed()) {
            score.setFirstTeamGoals(driver.findElement(By.xpath(String.format(TEAMS_XPATH + GOALS_XPATH, team1)))
                                    .getText());
            score.setSecondTeamGoals(driver.findElement(By.xpath(String.format(TEAMS_XPATH + GOALS_XPATH, team2)))
                                    .getText());
        } return score;
    }

    public void clickOnTeamsBlock(String team1, String team2) {
        driver.findElement(By.xpath(String.format(BLOCK_XPATH, team1, team2))).click();
    }
}

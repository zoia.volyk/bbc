package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import java.util.List;
import java.util.Map;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {
    private static final long DEFAULT_TIMEOUT = 60;

    WebDriver driver;
    HomePage homePage;
    NewsPage newsPage;
    CoronavirusPage coronavirusPage;
    CoronavirusStoriesPage coronavirusStoriesPage;
    QuestionsPage questionsPage;
    PageFactoryManager pageFactoryManager;
    SportPage sportPage;
    FootballPage footballPage;
    ScoresFixturesPage scoresFixturesPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @And("User opens home page")
    public void openPage() {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage();
    }


    @And("User clicks 'News' tab")
    public void clickNewsTab() {
        homePage.clickOnNewsTab();
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        newsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, newsPage.getRegisterPopup());
        newsPage.closePopup();
    }

    @And("User checks that name of headline article is {string}")
    public void checkNameOfHeadlineArticle(final String expectedName) {
        assertEquals(newsPage.getNameOfHeadlineArticle(), expectedName);
    }

    @And("User checks that secondary articles titles are correct")
    public void checksSecondaryArticlesTitles(List<String> secondaryArticlesTitles) {
        assertTrue(secondaryArticlesTitles.stream().allMatch(newsPage.getSecondaryArticlesTitlesList()::contains));
    }

    @And("User clicks 'Coronavirus' tab")
    public void clickCoronavirusTab() {
        newsPage.clickOnCoronavirusTab();
    }

    @And("User clicks 'Your Coronavirus Stories' tab")
    public void clickYourCoronavirusStoriesTab() {
        coronavirusPage = pageFactoryManager.getCoronavirusPage();
        coronavirusPage.clickOnYourCoronavirusStoriesTab();
    }
    
    @And("User clicks 'Your questions answered' link")
    public void clickYourQuestionsAnsweredLink() {
        coronavirusStoriesPage = pageFactoryManager.getCoronavirusStoriesPage();
        coronavirusStoriesPage.clickQuestionsToBbcLink();
    }

    @And("User fills form")
    public void fillForm(Map<String,String> values) {
        questionsPage = pageFactoryManager.getQuestionsPage();
        questionsPage.fillQuestionForm(values);
    }

    @And("User accepts terms of service")
    public void acceptTermsOfService() {
        questionsPage.clickAcceptCheckbox();
    }

    @And("User clicks 'Submit' button")
    public void clickSubmitButton() {
        questionsPage.clickSubmitButton();
    }

    @And("User checks that checkbox error message is {string}")
    public void checkCheckboxErrorMessage(final String errorMessage) {
        questionsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, questionsPage.getCheckboxErrorMessage());
        assertEquals(questionsPage.getTextOfCheckboxErrorMessage(), errorMessage);
    }

    @And("User checks that error message is {string}")
    public void checkInputErrorMessage(final String errorMessage) {
        questionsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, questionsPage.getInputErrorMessage());
        assertEquals(questionsPage.getTextOfInputErrorMessage(), errorMessage);
    }

    @And("User clicks 'Sport' tab")
    public void clickSportTab() {
        homePage.clickOnSportTab();
    }

    @And("User clicks 'Football' tab")
    public void clickFootballTab() {
        sportPage = pageFactoryManager.getSportPage();
        sportPage.clickOnFootballTab();
    }

    @And("User clicks 'Scores & Fixtures' tab")
    public void clickScoresFixturesTab() {
        footballPage = pageFactoryManager.getFootballPage();
        footballPage.clickOnScoresFixturesTab();
    }

    @And("User enters {string} in competition search bar")
    public void enterChampionshipInCompetitionSearchBar(String championship) {
        scoresFixturesPage = pageFactoryManager.getScoresFixturesPage();
        scoresFixturesPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, scoresFixturesPage.getCompetitionSearchField());
        scoresFixturesPage.enterTextToCompetitionSearchField(championship);
    }

    @And("User selects results for {string}")
    public void selectResultsForMonth(String month) {
        scoresFixturesPage.clickOnMonthTab(month);
    }

    @And("User checks that {string} and {string} played with {string} {string} score")
    public void checksScoreOfTwoTeams(String firstTeam, String secondTeam,
                                      String firstTeamGoals, String secondTeamGoals) {
        scoresFixturesPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, scoresFixturesPage.getYellowBlock());
        assertEquals(scoresFixturesPage.getScore(firstTeam, secondTeam).getFirstTeamGoals(), firstTeamGoals);
        assertEquals(scoresFixturesPage.getScore(firstTeam, secondTeam).getSecondTeamGoals(), secondTeamGoals);
    }

    @And("User clicks {string}{string} block")
    public void clickTeamsBlock(String firstTeam, String secondTeam) {
        scoresFixturesPage.clickOnTeamsBlock(firstTeam, secondTeam);
    }
}
